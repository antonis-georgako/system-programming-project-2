#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/select.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#define perror2(s,e) fprintf(stderr, "%s: %s\n", s, strerror(e))
#define MAX(a,b) (((a)>(b))?(a):(b))

//gia thn entolh STATS
int pages=0;
int bytes=0;
time_t start;

char root_dir[150];

///struct tha exei mesa file descriptors
struct node {
	int fd; //o fd tou socket pou tha diabasei kapoio thread
	struct node * next;
} *head = NULL;

struct node *p;

int end=0;

int numOfThreads,newsock;
pthread_t *tids; //id twn thread
int command_port_fd, serving_port_fd; 










pthread_mutex_t pages_bytes = PTHREAD_MUTEX_INITIALIZER; //gia pages & bytes
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER; //gia thn lista
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;

//synarthsh thread
void *starting_function(void* argp){
	struct node *p;
	int err;
	while(1){





		if (err = pthread_mutex_lock(&mutex)){
			perror2("pthread_mutex_lock", err);
		}

		while (head == NULL)
			pthread_cond_wait(&cond, &mutex);


		

		if(end == 1){
			printf("Thread %lu exiting...\n",pthread_self() );
			pthread_mutex_unlock(&mutex);
			pthread_exit(NULL);

		}








		p=head;
		head=head->next;
		int numofBytes;
		char character;
		char buf[512];
		char temp2[64];
		int i=0;
		
		while(1){
			if( read(p->fd, &character, 1) > 0) { /// Receive 1 char /
				buf[i]=character;
				
				if(i>4){
					if(buf[i-1] == '\r' && buf[i-3] == '\r' && buf[i-2] == '\n' && buf[i] == '\n'){
						break; //dld phra olo to http get
					}
				

				}
				i++;


			}

		}
		
		

		//gia na parw to arxeio
		char ** temp_command = NULL;
		char * part = strtok(buf," \t");
		int n_spaces = 0;



		while(part){
			temp_command = realloc(temp_command, sizeof(char*) * ++n_spaces);

			temp_command[n_spaces-1] = part;
			part = strtok (NULL," ");
		}

		temp_command = realloc(temp_command, sizeof(char *) *(n_spaces+1));
		temp_command[n_spaces] = 0;







		//temp_command[1] to arxeio

		char full_path[100];
		strcpy(full_path,root_dir);
		strcat(full_path,temp_command[1]);
		free(temp_command);
		free(part);

		//paw na dw an yparxei to arxeio kai mporw na exw prosbash
		struct stat st;
		int result = stat(full_path, &st);

		char answer[512]; //einai h apanthsh pou tha stalei pisw ston client
		

		///gia to date
		char date[1000];
		time_t now = time(0);
		struct tm tm = {0};
		tm = *localtime(&now);
		strftime(date, sizeof date, "%a, %d %b %Y %H:%M:%S %Z", &tm);




		printf("Returning answer for file: %s\n",full_path );


		///FILE NOT EXIST
		if(result != 0){
			printf("File doesn't exist\n");
			strcpy(answer, "HTTP/1.1 404 Not Found\r\nDate: ");
			strcat(answer, date);
			strcat(answer, "\r\nServer: myhttpd/1.0.0 (Ubuntu64)\r\nContent-Length: 124\r\nContent-Type: text/html\r\nConnection: Closed\r\n\r\n<html>Sorry dude, couldn't find this file.</html>");

			pthread_mutex_lock(&pages_bytes);
			bytes = bytes + strlen(answer);
			pthread_mutex_unlock(&pages_bytes);



				if(write(p->fd,answer,strlen(answer)) < 0){
					perror("write");
					break;
				}
			

		
		//FILE EXISTS
		}else{


		
			mode_t bits = st.st_mode;
			if((bits & S_IROTH) == 0){
				printf("Don't have permissions\n");
				strcpy(answer, "HTTP/1.1 403 Forbidden\r\nDate: ");
				strcat(answer, date);
				strcat(answer, "\r\nServer: myhttpd/1.0.0 (Ubuntu64)\r\nContent-Length: 124\r\nContent-Type: text/html\r\nConnection: Closed\r\n\r\n<html>Trying to access this file but don't think I can make it.</html>");


				pthread_mutex_lock(&pages_bytes);
				bytes = bytes + strlen(answer);
				pthread_mutex_unlock(&pages_bytes);



				if(write(p->fd,answer,strlen(answer)) < 0){
					perror("write");
					break;
				}





			}else{
				strcpy(answer, "HTTP/1.1 200 ΟΚ\r\nDate: ");
				strcat(answer, date);
				strcat(answer, "\r\nServer: myhttpd/1.0.0 (Ubuntu64)\r\nContent-Length: 100000\r\nContent-Type: text/html\r\nConnection: Closed\r\n\r\n");
				//edw tha paw na balw to arxeio mesa sta html tags
				long file_size = st.st_size;
				file_size = file_size + sizeof(answer)+10; //mazi me to </html>

				//char final_text[file_size];
				char final_text[100000];
				strcpy(final_text,answer);
				FILE *fp;
				fp = fopen(full_path,"r");
				char c;
				c=getc(fp);
				while(c != EOF)
				{
					strncat(final_text, &c,1);

					c = fgetc(fp);
				}
				fclose(fp);



				pthread_mutex_lock(&pages_bytes);
				bytes = bytes + strlen(final_text);
				pages = pages +1;
				pthread_mutex_unlock(&pages_bytes);




				if(write(p->fd,final_text,strlen(final_text)) < 0){
					perror("write");
					break;
				}






			}

		}







		close(p->fd);
		

		free(p);

	
		if (err = pthread_mutex_unlock(&mutex)){
			perror2("pthread_mutex_unlock", err);
		}


	}
	pthread_exit(NULL);
}


void execute_command(char* command);



int main(int argc, char *argv[]){

	start = time(0);




	///////////////////////////////////////
	///threads
	int err,command_port,serving_port;




	//////////////////////////////////////
	/////sockets
	struct sockaddr_in server, client;
	socklen_t clientlen;
	struct sockaddr *serverptr=(struct sockaddr *)&server;
	struct sockaddr *clientptr=(struct sockaddr *)&client;
	




	//elegxos orismatwn & apothikeysh
	if (argc != 9){
		printf("Error in arguments\n");
		return 1;
	}


	int opt;
	while ((opt = getopt (argc, argv, "p:c:t:d:")) != -1){
		switch (opt){
		case 'p':
			printf ("Serving port: %s\n", optarg);
			serving_port=atoi(optarg);
			break;
		case 'c':
			printf ("Command port: %s\n", optarg);
			command_port=atoi(optarg);
			break;
		case 't':
			printf ("Number of threads: %s\n", optarg);
			numOfThreads=atoi(optarg);
			break;
		case 'd':
			printf("Root dir: %s\n", optarg);
			strcpy(root_dir,optarg);
			break;


		}
	}



	////////////////////////////////////////////
	////////////////////////////////////////////
	//dhmiourgia thread pool





	if ((tids = malloc(numOfThreads * sizeof(pthread_t))) == NULL){
		perror("malloc");
		exit(1);
	}


	for (int i=0;i<numOfThreads;i++){
		if (err = pthread_create(tids+i,NULL,starting_function,NULL)){
			perror2("pthread_create", err);
			exit(1);
		}
	}











	////////////////////////////////////////////
	/////////////////////////////////////////
	////////sockets




	while(1){





		fd_set set;
		//FD_ZERO(&set);


		//COMMAND PORT///////////////////////////////

		server.sin_family=AF_INET;
		server.sin_addr.s_addr=htonl(INADDR_ANY);
		server.sin_port=htons(command_port);




		if ((command_port_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0){
			perror("socket");
			return 1;		
		}
				

		if (bind(command_port_fd, serverptr, sizeof(server)) < 0)
		{
			perror("bind");
			return 1;		
		}
		

		if (listen(command_port_fd, 128) < 0){
			perror("listen");
			return 1;		
		}
		clientlen = sizeof(client);







		//SERVING PORT ///////////////////
		server.sin_port=htons(serving_port);

		if ((serving_port_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0){
			perror("socket");
			return 1;		
		}
				

		if (bind(serving_port_fd, serverptr, sizeof(server)) < 0)
		{
			perror("bind");
			return 1;		
		}
		

		if (listen(serving_port_fd, 128) < 0){
			perror("listen");
			return 1;		
		}
		clientlen = sizeof(client);











		for(;;){





			FD_ZERO(&set);


			FD_SET(command_port_fd, &set);

			FD_SET(serving_port_fd, &set);



			int smax=MAX(command_port_fd,serving_port_fd)+1;
			int result;




			result = select(smax, &set, NULL, NULL, NULL);

			if(result <0){
				perror("select");
				break;
			}else if(result > 0){



				//an exoume syndesh gia serving port, prosthetoume 1 epipleon fd ston shared buffer
				if (FD_ISSET(serving_port_fd, &set)){
					pthread_mutex_lock(&mutex);
					p=(struct node*)malloc(sizeof(struct node));
					p->next=NULL;
					if(head == NULL)
						head=p;
					if (( (p->fd) = accept(serving_port_fd, clientptr, &clientlen))< 0){
						perror("accept");
						close(command_port_fd);
						close(serving_port_fd);
						close(p->fd);
						pthread_mutex_unlock(&mutex);
						break;
					}
					//printf("Putting file descriptor in list\n");
					p=p->next;
					pthread_cond_signal(&cond);
					pthread_mutex_unlock(&mutex);



				}
				





				//an exoume syndesh gia command_port
				if (FD_ISSET(command_port_fd, &set)){
					if ((newsock = accept(command_port_fd, clientptr, &clientlen))< 0){
						perror("accept");
						close(command_port_fd);
						close(serving_port_fd);
						close(newsock);

						break;
					}
					printf("Getting input from command_port\n");

					char buf[100];

					while( read(newsock, buf, 100) > 0) {
						execute_command(buf);
					}

				}




			}//an result>0





		} //tou for









	}
	return 0;


}





void execute_command(char* command){

	time_t elapsed = difftime(time(0), start);



	char temp_command[100];
	sscanf(command,"%s",temp_command);

		
	if(strcmp(temp_command,"STATS") == 0){
		pthread_mutex_lock(&pages_bytes);
		printf("Server up for %ld:%ld.%ld , served %d pages, %d bytes\n",(elapsed/60),(elapsed%60),(elapsed*1000)%60,pages,bytes);
		pthread_mutex_unlock(&pages_bytes);



	}else if(strcmp(temp_command,"SHUTDOWN") == 0){
		printf("Shuting down server\n");
		end=1;


		////edw tha baloume kati sto head
		pthread_mutex_lock(&mutex);
		if(head == NULL){
			head = (struct node*)malloc(sizeof(struct node));

		}
		pthread_mutex_unlock(&mutex);
		for(int i=0;i<numOfThreads;i++)
			pthread_cond_signal(&cond);
			
		for (int i = 0; i < numOfThreads; i++) {
		    pthread_join(tids[i], NULL);
		}

		close(command_port_fd);
		close(serving_port_fd);		
		close(newsock);
		free(tids);

		//free struct node
		struct node * current = head;
		while(current != NULL){
			struct node *temp=current;
			current=current->next;
			free(temp);
		}
		free(current);

		struct node * current1 = p;
		while(current1 != NULL){
			struct node *temp=current1;
			current1=current1->next;
			free(temp);
		}
		free(current1);

		pthread_mutex_destroy(&mutex);
		pthread_mutex_destroy(&pages_bytes);


	}




	return;
}
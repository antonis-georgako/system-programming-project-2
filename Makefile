SOURCE = myhttpd.c
OBJECTS = myhttpd.o
CC = gcc
CFLAGS = -Wall -g

all: myhttpd

#separate compilation

myhttpd: $(OBJECTS)
	$(CC) $(CFLAGS) $(OBJECTS) -o myhttpd -pthread	

	

myhttpd.o: myhttpd.c
	$(CC) $(CFLAGS) -c myhttpd.c -pthread



clean:
	-rm $(OBJECTS)
	-rm myhttpd

count:
	wc $(SOURCE)

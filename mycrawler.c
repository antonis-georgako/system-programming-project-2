#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/select.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <netdb.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <ctype.h>
#include <stdbool.h>
#include <netdb.h>
#define perror2(s,e) fprintf(stderr, "%s: %s\n", s, strerror(e))
#define PERMS 0755

//gia thn entolh STATS
int pages=0;
int bytes=0;
time_t start;
pthread_t *tids; //id twn thread
int end=0; //stamatane ta threads


int command_port,numOfThreads,command_port_fd;
int new_sock;

void jobExecutor(char * command);
void execute_command(char* command);
bool isNumber(char number[]);
int hostname_to_ip(char * hostname , char* ip);
bool directory_exists(char * directory);

/////gia to socket

struct hostent *h;
int server_port;
int sock;
struct sockaddr_in server;
struct sockaddr *serverptr = (struct sockaddr*)&server;


pthread_mutex_t pages_bytes = PTHREAD_MUTEX_INITIALIZER; //gia pages & bytes

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

///struct gia ta links
struct node {
	char links[128];
	struct node * next;
};
struct node *head = NULL;


struct node * current; //autos pou tha koitame kathe fora
char save_dir[100];



/////////threads
void *starting_function(void* argp){



	int err;
	while(1){

		if (err = pthread_mutex_lock(&mutex)){
			perror2("pthread_mutex_lock", err);
		}

		if(end == 1){
			printf("Thread %lu exiting...\n",pthread_self() );
			pthread_mutex_unlock(&mutex);
			pthread_exit(NULL);
		}


		if (current != NULL){
			

			////////////////////////////////////
			/////////////////////////////////////
			//////////socket
			if((sock = socket(AF_INET,SOCK_STREAM,0)) < 0){
				perror("socket");
				exit(1);
			}
			server.sin_family = AF_INET;
			server.sin_port = htons(server_port);
			memcpy(&server.sin_addr,h->h_addr,h->h_length);




			//////////////////////////////////////////
			///////////////////////////////////////////
			/////////////////send GET REQUEST
			char get_request[512];
			strcpy(get_request, "GET ");
			strcat(get_request, current->links);
			strcat(get_request, " HTTP/1.1\r\nUser-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\nHost: www.tutorialspoint.com\r\nAccept-Language: en-us\r\nAccept-Encoding: gzip, deflate\r\nConnection: Keep-Alive\r\n\r\n");

			//connect & send ston server
			if(connect(sock,serverptr,sizeof(server)) < 0){
				perror("connect");
				pthread_mutex_unlock(&mutex);
					pthread_exit(NULL);

				//continue;
			}

			int i=0;
			while(i<strlen(get_request)){
				if(write(sock,get_request+i,1) < 0){
					perror("write");
					pthread_mutex_unlock(&mutex);
					pthread_exit(NULL);

					//continue;
				}
				i++;
			}

			i=0;
			char buf[100000]={0};
			char temp[100000]={0};

			int metr_buf=0;
			char temp2[64]={0};
			/////////diabasma se chunks
			int numofBytes;
			while((numofBytes=read(sock,temp2,64))>0){
				for(int i=0;i<numofBytes;i++){
					buf[metr_buf]=temp2[i];
					metr_buf++;
				}
			}


			strncpy(temp,buf,strlen(buf));
			int megethos=0;
			megethos=strlen(buf);




			pthread_mutex_lock(&pages_bytes);
			bytes = bytes + megethos;
			pthread_mutex_unlock(&pages_bytes);
			///////////////////////////////////////
			///////////////////////////////////////
			///////////save file in save_dir



			//1) elegxos an ok, notfound , denied
			char ** temp_command = NULL;
			char * part = strtok(temp," \t");
			int n_spaces = 0;



			while(part){
				temp_command = realloc(temp_command, sizeof(char*) * ++n_spaces);

				temp_command[n_spaces-1] = part;
				part = strtok (NULL," ");
			}

			temp_command = realloc(temp_command, sizeof(char *) *(n_spaces+1));
			temp_command[n_spaces] = 0;



			free(part);
			int code = atoi(temp_command[1]);
			free(temp_command);

			//2) exoume arxeio, pame na to apothikeusoume sto save_dir
			if (code == 200){
	

				pthread_mutex_lock(&pages_bytes);
				pages = pages +1;
				pthread_mutex_unlock(&pages_bytes);

				

				char directory_file[24];
				strcpy(directory_file, current->links);				

				///elegxos an yparxei directory, arxeio me to onoma t
				char ** temp_command = NULL;
				part = strtok(directory_file,"/");
				n_spaces = 0;



				while(part){
					temp_command = realloc(temp_command, sizeof(char*) * ++n_spaces);

					temp_command[n_spaces-1] = part;
					part = strtok (NULL,"/");
				}

				temp_command = realloc(temp_command, sizeof(char *) *(n_spaces+1));
				temp_command[n_spaces] = 0;




				char full_path[100]={0};

				///////////elegxos an yparxei dir. an uparxei apla apothik arxeiou
				if(directory_exists(temp_command[0])){
					strcpy(full_path,save_dir);
					strcat(full_path,"/");
					strcat(full_path,temp_command[0]);
					strcat(full_path,"/");
					strcat(full_path,temp_command[1]);
					int file_desc = open(full_path,  O_CREAT, PERMS);
					close(file_desc);


				}else{ //den yparxei, pame na to ftiaxoume

					strcpy(full_path,save_dir);
					strcat(full_path,"/");
					strcat(full_path,temp_command[0]);
					
					
					mkdir(full_path, PERMS);

					strcat(full_path,"/");
					strcat(full_path,temp_command[1]);
					int file_desc = open(full_path, O_CREAT, PERMS);
					close(file_desc);
				}
				printf("Saving file: %s\n",full_path);

				//////////////////////////////////////////////
				/////////////frees
				free(part);
				free(temp_command);



				FILE *fptr1;
				fptr1 = fopen(full_path, "w");

				if(fptr1 == NULL){
					perror("FOPEN");
					pthread_mutex_unlock(&mutex);
					pthread_exit(NULL);
				}

				int j;
				for(j=0;j<megethos; j++){
					if(buf[j] == '<' && buf[j+1] == '!' && buf[j+2] == 'D' && buf[j+3] == 'O' && buf[j+4] == 'C')
						break;
				}

				while(j<megethos){
					fprintf(fptr1,"%c",*(buf+j));
					j++;
				}
				////molis apothikeuse to arxeio me ta periexomena tou sto save_dir
				fclose(fptr1);


				/////efoson exoume to arxeio, to "scanaroume" gia epipleon links

				j=0;
				char temp_link[23];
				int metrhths_link=0;
				while(j<(megethos-167)){
					if(buf[j] == '<' && buf[j+1] == 'a' && buf[j+2] == ' ' && buf[j+3] == 'h' && buf[j+4] == 'r' && buf[j+5] == 'e' && buf[j+6] == 'f' && buf[j+7] == '=')
					{
						int flag=0;
						while(flag==0){
							if(buf[j] == '/' && buf[j+1] == 's' && buf[j+2] == 'i' && buf[j+3] == 't' && buf[j+4] == 'e'){
								metrhths_link=0;
								while(buf[j] != '>'){
									temp_link[metrhths_link]=buf[j];
									j++;
									metrhths_link++;
								}
								flag=1;
								temp_link[metrhths_link]='\0';

								//////////////////////////////////////////
								//////////////////////////////////////////								
								////elegxos an uparxei mesa sthn lista mas
								struct node * temp_node;
								temp_node=head;
								while( (temp_node->next) != NULL){
									if(strcmp(temp_node->links,temp_link) == 0){ //yparxei hdh
										break;
									}
									temp_node=temp_node->next;
								}

								if((temp_node->next) == NULL){
									if(strcmp(temp_node->links,temp_link) != 0){ //den uparxei
										struct node * new_temp;
										new_temp =(struct node *) malloc(sizeof(struct node));
										strcpy(new_temp->links,temp_link);
										new_temp->next=NULL;
										temp_node->next = new_temp;
										//free(new_temp);
									}
								}


							}
							j++;
						}
					
					
					}
					j++;
				
				}


	

			}else if(code == 403){
				printf("I don't have the permission for this file\n");
			}else{
				printf("File doesn't exist\n");
			}

			close(sock);
		}


			if(current!=NULL){
				if(current->next != NULL){
					current=current->next;
				}else{
					current=NULL;
				}
			}

			



		if (err = pthread_mutex_unlock(&mutex)){
			perror2("pthread_mutex_unlock", err);
		}

		
	}
	pthread_exit(NULL);
}








int main(int argc, char *argv[]){
	start = time(0);

	
	int err;
	char starting_URL[100];
	char server_IP[15];




	//elegxos orismatwn & apothikeysh
	if (argc != 12){
		printf("Error in arguments\n");
		return 1;
	}


	int opt;
	while ((opt = getopt (argc, argv, "h:p:c:t:d:")) != -1){
		switch (opt){
		case 'h':
		//elegxos an mou exei dwsei ip h host name
			if(isNumber(optarg)){
				strcpy(server_IP,optarg);
				printf("H ip einai:  %s \n", server_IP);
			}else{

				if((h = gethostbyname(optarg) ) == NULL){
					herror("gethostbyname");
					exit(1);
				}
				
			}
			
			break;
		case 'p':
			printf ("Server port: %s\n", optarg);
			server_port=atoi(optarg);
			break;
		case 'c':
			printf ("Command port: %s\n", optarg);
			command_port=atoi(optarg);
			break;
		case 't':
			printf("Number of threads: %s\n", optarg);
			numOfThreads=atoi(optarg);
			break;
		case 'd':
			printf("Directory for save: %s\n", optarg);
			strcpy(save_dir,optarg);
			break;


		}
	}
	strcpy(starting_URL,argv[11]);
	printf("Starting URL: %s\n", starting_URL);
	starting_URL[strlen(starting_URL)+1]='\0';



	/////////////////////////////////////////////
	/////////////////////////////////////////////
	//////apothikeush prwtou link gia na ksekinhsoume

	int i;
	///kanoume extract to directory kai to arxeio apo to url
	for(i=0;i<strlen(starting_URL);i++){
		if(starting_URL[i] == '/' && starting_URL[i+1] == 's' && starting_URL[i+2] == 'i'){
			break;
		}
	}

	int j=0;
	char link[100];
	while(i<strlen(starting_URL)){
		link[j] = starting_URL[i];
		i++;
		j++;
	}
	link[j]='\0';




	//apothikeush prwtou link sthn lista mas
	
	head = (struct node*)malloc(sizeof(struct node));
	strcpy(head->links,link);
	head->next=NULL;	
	
	current=head;
	//// ara edw ftiaxame to prwto stoixeio ths listas mas


	


	////////////////////////////////////////////
	////////////////////////////////////////////
	//dhmiourgia thread pool





	if ((tids = malloc(numOfThreads * sizeof(pthread_t))) == NULL){
		perror("malloc");
		exit(1);
	}


	for (int i=0;i<numOfThreads;i++){
		if (err = pthread_create(tids+i,NULL,starting_function,NULL)){
			perror2("pthread_create", err);
			exit(1);
		}
	}



	////////////////////////////////////////
	////////////////////////////////////////
	///////////COMMAND PORT

	struct sockaddr_in server2,client;

	struct sockaddr *serverptr2 = (struct sockaddr*)&server2;

	server2.sin_family=AF_INET;
	server2.sin_addr.s_addr=htonl(INADDR_ANY);
	server2.sin_port=htons(command_port);
	struct sockaddr *clientptr=(struct sockaddr *)&client;
	socklen_t clientlen;

	if ((command_port_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0){
		perror("socket");
		return 1;		
	}
				

	if (bind(command_port_fd, serverptr2, sizeof(server2)) < 0)
	{
		perror("bind");
		return 1;		
	}
		

	if (listen(command_port_fd, 128) < 0){
		perror("listen");
		return 1;		
	}


	while(1){
		clientlen = sizeof(client);
		if ((new_sock = accept(command_port_fd, clientptr, &clientlen))< 0){
			perror("accept");
			exit(1);
		}

		char buf[100];
		printf("Enter your command\n");
		while( read(new_sock, buf, 100) > 0) { /// Receive 1 char /
			execute_command(buf);
		}


	}
				






	return 0;
}



//elegxos an ip h host
bool isNumber(char number[])
{

    for (int i=0; number[i] != 0; i++)
    {
        if (isdigit(number[i]) || number[i] == '.'){
        	
        }else{
        	return false;
        }
    }
    return true;


}






bool directory_exists(char * directory){
	struct stat s;
	char full_path[100];
	strcpy(full_path,save_dir);
	strcat(full_path,"/");
	strcat(full_path,directory);

	int err = stat(full_path, &s);
	if(err == -1){
		return false;
	}else{
		return true;
	}

}



void execute_command(char* command){

	//gia date
	time_t elapsed = difftime(time(0), start);



	char temp_command[100];
	sscanf(command,"%s",temp_command);

	

	//////////////////STATS
	////////////////////////////	
	if(strcmp(temp_command,"STATS") == 0){
		pthread_mutex_lock(&pages_bytes);
		printf("Crawler up for %ld:%ld.%ld , downloaded %d pages, %d bytes\n",(elapsed/60),(elapsed%60),(elapsed*1000)%60,pages,bytes);
		pthread_mutex_unlock(&pages_bytes);



	
	//////////////////SHUTDOWN
	////////////////////////////////
	}else if(strcmp(temp_command,"SHUTDOWN") == 0){
		printf("Shuting down crawler\n");
		end=1;
		for (int i = 0; i < numOfThreads; i++) {
		    pthread_join(tids[i], NULL);  
		}
		close(command_port_fd);
		close(new_sock);
		free(tids);

		//free struct node
		current=head;
		while(current != NULL){
			struct node *temp=current;
			current=current->next;
			free(temp);
		}
		free(current);
		pthread_mutex_destroy(&mutex);
		pthread_mutex_destroy(&pages_bytes);




		exit(0);


	///////////SEARCH
	////////////////////////////////
	}else{
		//spaw entolh kai blepw an 1o "kommati" einai search
		char temp_command1[100];
		strcpy(temp_command1,command);
		char ** temp_command = NULL;
		char * p = strtok(temp_command1," \n");
		int n_spaces = 0;



		while(p){
			temp_command = realloc(temp_command, sizeof(char*) * ++n_spaces);

			temp_command[n_spaces-1] = p;
			p = strtok (NULL," ");
		}

		temp_command = realloc(temp_command, sizeof(char *) *(n_spaces+1));
		temp_command[n_spaces] = 0;


		if(strcmp(temp_command[0],"SEARCH") == 0){
			if( current != NULL){
				printf("Crawling in progress...\n");

			}else{
				printf("Executing Search..\n");








				/////////////////////////////////////////////////////
				/////////////////////////////////////////////////////
				///////////////////////1h periptwsh
				///////dhmiourgia neou socket

				///estw port 8888
/*
				int new_sock1;


				if((new_sock1 = socket(AF_INET,SOCK_STREAM,0)) < 0){
					perror("socket");
					exit(1);
				}
				server.sin_family = AF_INET;
				server.sin_port = htons(8888);
				server.sin_addr.s_addr=htonl(INADDR_ANY);



				if(connect(new_sock1,serverptr,sizeof(server)) < 0){
					perror("connect");
					exit(1);
					//continue;
				}


				int i=0;
				while(i<strlen(command)){
					if(write(new_sock1,command+i,1) < 0){
						perror("write");
						exit(1);
						//continue;
					}
					i++;
				}

				printf("\n");

				char temp_buf[100];
				i=0;
				while(read(new_sock1,temp_buf+i,1)>0){
				
					i++;
				}
				temp_buf[i+1]='\0';
				printf("Got back: %s\n",temp_buf );



				close(new_sock1);
*/



				///////////////////////////////////////////////////
				///////////////////////////////////////////////////
				//////////////////////2h periptwsh


				////call synarthsh "jobExecutor"
				jobExecutor(command);
















			}
		}else{
			printf("Invalid argument\n");
		}

		free(p);
		free(temp_command);





	}




	return;
}




void jobExecutor(char * command){

			char ** temp_command = NULL;
			char * p = strtok(command," \n");
			int n_spaces = 0;



			while(p){
				temp_command = realloc(temp_command, sizeof(char*) * ++n_spaces);

				temp_command[n_spaces-1] = p;
				p = strtok (NULL," ");
			}

			temp_command = realloc(temp_command, sizeof(char *) *(n_spaces+1));
			temp_command[n_spaces] = 0;

			int i=1;
			while(temp_command[i] != NULL){
				printf("Searching for: %s\n",temp_command[i] );
				i++;
			}

			free(temp_command);
			free(p);



			printf("Sending answers through socket\n");
			char answer[] = "These are the results of search.....\n";
			if(write(new_sock,answer,strlen(answer)) < 0){
					perror("write");
					close(sock);
					exit(1);
			}


}
#!/bin/bash

#elegxos orismatwn
if [ $# -ne 4 ] 
then
	echo "Not enough arguments. Exiting..."
	exit 1
fi

#apothikeush timwn
root_dir=$1
text_file=$2
w=$3
p=$4


#elegxw an katalogos & text_file einai valid
if ! [ -d $root_dir ]
then
	echo "root_directory doesnt exist"
	exit 1
fi

if ! [ -e $text_file ]
then
	echo "text_file doesnt exist"
	exit 1
fi





#diagrafh arxeiwn sto root_directory ama yparxoun
temp=$(ls $root_dir)

if [ -n "$temp" ]
then
	echo "Warning: directory is full, purging..." 
	rm -rf $root_dir/*
fi


#elegxos an grammes sto text_file einai panw apo 10.000
lines=`wc -l $text_file | cut -f1 -d' '`
if [ $lines -lt 10 ]
then
	echo "Not enough lines in textfile"
	exit 1
fi

#elegxos an arithmoi w kai p einai integers
re='^[0-9]+$'
if ! [[ $w =~ $re ]] ; then
   echo "error: Not a number" >&2; exit 1
fi

re='^[0-9]+$'
if ! [[ $p =~ $re ]] ; then
   echo "error: Not a number" >&2; exit 1
fi


#dhmiourgia w katalogwn sto root_directory
metrhths=0
for ((i=0;i < $w ; i++))
do
	mkdir "$root_dir/site$i"
	echo "Creating web site $i"
	#kai dhmiourgia arxeiwn-pages ston kathe katalogo
	random_numbers=`shuf -i 1-20000 -n $p`
	for j in $random_numbers
	do
		filename="$root_dir/site$i/page$i"_"$j.html"
		touch $filename
		istoselides[$metrhths]=$filename
		metrhths=$((metrhths+1))
		#touch "$root_dir/site$i/page$i"_"$j.html"
	done


done



#gia ta periexomena kathe istoselidas akolouthoume ta 8 bhmata
first_headers="<!DOCTYPE html><html><body>"
last_headers="</body></html>"



#ta positions deixnoun apo pou mexri pou tha paroume ston pinaka istoselides
position1=0
let "position2=$p-1"
metrhths=0
array_len=${#istoselides[@]}
let "array_len=$array_len-1"



#ftiaxnw deytero pinaka gia na dw an ola ta pages exoun incoming link
#ta gemizoume ola 0 ypothetontas oti dn exoun incoming links
for ((r=0;r <= $array_len ; r++))
do
	let "incoming[$r]=0"
done






#mia megalh loop gia oles tis istoselides
for i in "${istoselides[@]}"
do



	let "temp=$lines-2001"
	k=`shuf -i 2-$temp -n 1`
	m=`shuf -i 1001-1999 -n 1`
	let "f=($p/2)+1"
	let "q=($w/2)+1"

	metrhths_eswt=0 #synolo f
	metrhths_ekswt=0 #synolo q

	#dhmiourgia synolou f istoselidwn
	let "thesi1=$metrhths-1"
	let "thesi2=$metrhths+1" #deixnoun ta shmeia apo pou ews pou tha paroun times apo ton pinaka istoselides

	let "f1=$f/2"
	let "f2=$f/2"
	let "f3=$f%2"
	let "f2=$f2+$f3"





	#eswterika links
	if [ $metrhths -eq $position1 ] ; then
		for index in $(shuf --input-range=$thesi2-$position2 -n $f)
		do
		    eswterika_links[$metrhths_eswt]=${istoselides[$index]}
		    metrhths_eswt=$((metrhths_eswt+1))

		done
	elif [ $metrhths -eq $position2 ] ; then
		for index in $(shuf --input-range=$position1-$thesi1 -n $f)
		do
		    eswterika_links[$metrhths_eswt]=${istoselides[$index]}
		    metrhths_eswt=$((metrhths_eswt+1))

		done
	else
		for index in $(shuf --input-range=$position1-$thesi1 -n $f1)
		do
		    eswterika_links[$metrhths_eswt]=${istoselides[$index]}
		    metrhths_eswt=$((metrhths_eswt+1))

		done

		for index in $(shuf --input-range=$thesi2-$position2 -n $f2)
		do
		    eswterika_links[$metrhths_eswt]=${istoselides[$index]}
		    metrhths_eswt=$((metrhths_eswt+1))

		done

	fi




	#gia ta ekswterika links
	let "q1=$q/2"
	let "q2=$q/2"
	let "q3=$q%2"
	let "q2=$q2+$q3"
	let "boith1=$position2+1"
	let "boith2=$position1-1"

	if [ $position1 -eq 0 ]
	then
		for index in $(shuf --input-range=$boith1-$array_len -n $q)
		do
		    ekswterika_links[$metrhths_ekswt]=${istoselides[$index]}
		    metrhths_ekswt=$((metrhths_ekswt+1))

		done

	
	elif [ $position2 -eq $array_len ]
	then
		for index in $(shuf --input-range=0-$boith2 -n $q)
		do
		    ekswterika_links[$metrhths_ekswt]=${istoselides[$index]}
		    metrhths_ekswt=$((metrhths_ekswt+1))

		done

	
	else
		#eimaste se p-ada anamesa oute sthn arxh oute sto telos
		for index in $(shuf --input-range=0-$boith2 -n $q1)
		do
		    ekswterika_links[$metrhths_ekswt]=${istoselides[$index]}
		    metrhths_ekswt=$((metrhths_ekswt+1))

		done

		for index in $(shuf --input-range=$boith1-$array_len -n $q2)
		do
		    ekswterika_links[$metrhths_ekswt]=${istoselides[$index]}
		    metrhths_ekswt=$((metrhths_ekswt+1))

		done

	fi




	#pame na baloume tis grammes tou arxeiou sthn html istoselida 


	#arxika headers
	echo "<!DOCTYPE html><html><body>" >> $i


	let "paketo_grammwn=$m/($f+$q)" #dhladh ana toses grammes kai apo ena link
	let "ypoloipa=$m%($f+q)"
	let "athroisma=$f+$q-1" #dhladh tha deixnei sto teleytaio paketo grammwn 
	array_len_eswt=${#eswterika_links[@]}
	let "array_len_eswt=$array_len_eswt-1"
	array_len_ekswt=${#ekswterika_links[@]}
	let "array_len_ekswt=$array_len_ekswt-1"



	cnt=0 #metrhths grammhs
	counter_a=0 #poses grammes exw balei sto link
	counter_b=0 #ama exw kseperasei to teleytaio paketo ara tha prepei na balw tis "ypoloipes" grammes
	counter_c=0 #gia tis "ypoloipes" pou tha mpoun sto telos
	metrhths_ekswt=0
	metrhths_eswt=0



	echo "Creating page $i with $m lines starting at line $k"




	while read LINE
	do

		#otan brethoume sthn grammh p theloume
	    if [ "$cnt" -ge $k ];
	    then


	    	if [ $counter_a -lt $paketo_grammwn -a $counter_b -lt $athroisma ] #synexizoume na bazoume lines
	    	then
	    		echo $LINE >> $i #antigrafh grammhs sthn istoselida
	        	counter_a=$((counter_a+1))
	        elif [ $counter_a -ge $paketo_grammwn ] #an exoume topothethsei oles tis grammes tou "paketou" pame na baloume ena link dipla
	        then
	        	counter_a=0
	        	if [ $metrhths_eswt -lt $array_len_eswt ] #topothetoume ena eswteriko link
	        	then
	        		echo "<a href=${eswterika_links[$metrhths_eswt]}>link"$cnt"_text</a>" >> $i
	        		echo "Adding link to ${eswterika_links[$metrhths_eswt]}"

	        		#ftiaxnw incoming links
	        		for ((r=0 ; r <= $array_len ; r++))
	        		do
	        			if [  ${istoselides[$r]} == ${eswterika_links[$metrhths_eswt]} ]
	        			then
	        				let "incoming[$r]=1"
	        				break
	        			fi
	        		done



	        		metrhths_eswt=$((metrhths_eswt+1))
	        		echo "<br>" >> $i
	        		counter_b=$((counter_b+1))
	        	elif [ $metrhths_ekswt -lt $array_len_ekswt ]
				then
	        		echo "<a href=${ekswterika_links[$metrhths_ekswt]}>link"$cnt"_text</a>" >> $i
	        		echo "Adding link to ${ekswterika_links[$metrhths_ekswt]}"

					for ((r=0 ; r <= $array_len ; r++))
	        		do
	        			if [  ${istoselides[$r]} == ${ekswterika_links[$metrhths_ekswt]} ]
	        			then
	        				
	        				let "incoming[$r]=1"
	        				break
	        			fi
	        		done





	        		metrhths_ekswt=$((metrhths_ekswt+1))
        			echo "<br>" >> $i
        			counter_b=$((counter_b+1))
        			if [ $metrhths_ekswt -ge $array_len_ekswt ] ; then
        				break
        			fi
				fi
			elif [ $counter_b -ge $athroisma ] #eimaste sto teleytaio set grammwn ara tha baloume tis "ypoloipes" grammes pou perisepsan apo thn diairesh
			then
				if [ $counter_c -lt $ypoloipa ]
				then
					echo $LINE >> $i
					counter_c=$((counter_c+1))
				else #teleiwsame me tis 'ypoloipes' grammes
					counter_b=0
				fi

			fi


	    fi
	    cnt=$((cnt+1))
	done < $text_file












	
	echo "</body></html>" >> $i







: '
	for i in "${eswterika_links[@]}"
	do
		echo $i
	done


	for i in "${ekswterika_links[@]}"
	do
		echo $i
	done
'

	let "metrhths=$metrhths+1"
	#tha prepei na "deiksoume" sthn epomenh p-ada istoselidwn
	if [ $metrhths -gt $position2 ]
	then
		let "position1=$position1+$p"
		let "position2=$position2+$p"
	fi


done



flag=0
for ((r=0;r <= $array_len ; r++))
do
	if [ ${incoming[$r]} -eq 0 ]
	then
		let "flag=1"
	fi
done
if [ $flag -eq 0 ]
then
	echo "All pages have at least one incoming link"
fi

echo "Done."